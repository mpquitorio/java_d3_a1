import com.zuitt.app.Customer;
import com.zuitt.app.Date;
import com.zuitt.app.Staff;
import com.zuitt.app.User;
import java.util.Scanner;

public class Main {
    // main method begins execution for Java applications
    public static void main( String[] args ) {

        String userName;
        String userCategory;
        String menuChoice;

        // create a Scanner to obtain input from the command window
        Scanner input = new Scanner( System.in );

        System.out.print( "Enter your name: " ); // prompt
        userName = input.nextLine();

        // Input only -> Staff or Customer
        System.out.print( "Enter your category: " ); // prompt
        userCategory = input.nextLine();

        Date date = new Date(9,10,2020);

        User user = new User( userName, userCategory, date );

        System.out.printf( "\nRegistering %s... \n\n", user.getName() );
        System.out.println( "Welcome to Foods Asia Store!" );
        System.out.printf( "%s, you are now logged in today, %s. \n\n", user.getName(), date.displayDate() );

        if( user.getCategory().equals( "Staff" ) ) {
            Staff s = new Staff( user.getName(), user.getCategory(), date );
            s.displayMenus();
            System.out.print( "Choose your menu to continue...  " ); // prompt
            menuChoice = input.nextLine();
            user.setMenuChoice( menuChoice );

            switch ( user.getMenuChoice() ) {
                case "A" -> s.displayCustomerList();
                case "B" -> s.displayProductList();
                case "C" -> user.displayLogoutMessage( user.getName() );
                default -> System.out.println("System error... out of range...");
            }
        } else {
            Customer c = new Customer( user.getName(), user.getCategory(), date );
            c.displayMenus();
            System.out.print( "Choose your menu to continue...  " ); // prompt
            menuChoice = input.nextLine();
            user.setMenuChoice( menuChoice );

            switch ( user.getMenuChoice() ) {
                case "A" -> c.displayCustomerOrders();
                case "B" -> c.displayCustomerTransactions();
                case "C" -> user.displayLogoutMessage( user.getName() );
                default -> System.out.println("System error... out of range...");
            }
        }

    } // end method main
} // end class Main