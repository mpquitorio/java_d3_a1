package com.zuitt.app;

public class Staff extends User {

    public Staff(String name, String category, Date dateLogin) {
        super(name, category, dateLogin);
    }

    public void displayMenus() {
        System.out.println( "MAIN MENU" );
        System.out.printf( "%s %15s %15s \n\n", "A. Customers", "B. Products", "C. Logout" );
    }

    public void displayCustomerList() {
        System.out.println( "\nMANAGE CUSTOMERS ");
        System.out.println( "This section is under maintenance." );
    }

    public void displayProductList() {
        System.out.println( "\nMANAGE PRODUCTS ");
        System.out.println( "This section is under maintenance." );
    }

} // end class Staff