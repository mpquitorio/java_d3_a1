package com.zuitt.app;

public class Customer extends User {

    public Customer(String name, String category, Date dateLogin) {
        super(name, category, dateLogin);
    }

    public void displayMenus() {
        System.out.println( "MAIN MENU" );
        System.out.printf( "%s %20s %15s \n\n", "A. Orders", "B. Transactions", "C. Logout" );
    }

    public void displayCustomerOrders() {
        System.out.println( "\nMY ORDERS ");
        System.out.println( "This section is under maintenance." );
    }

    public void displayCustomerTransactions() {
        System.out.println( "\nVIEW TRANSACTIONS ");
        System.out.println( "This section is under maintenance." );
    }

} // end class Customer