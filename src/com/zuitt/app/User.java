package com.zuitt.app;

public class User {

    private final String name;
    private final String category;
    private String menuChoice;
    // composition
    private final Date loginDate;

    // constructor
    public User( String name, String category, Date dateLogin ) {
        this.name = name;
        this.category = category;
        this.loginDate = dateLogin;
    }

    // getters
    public String getName() { return this.name; }
    public String getCategory() { return this.category; }
    public String getMenuChoice() { return menuChoice; }

    // setters
    public void setMenuChoice( String menuChoice ) { this.menuChoice = menuChoice; }

    public void displayLogoutMessage( String name ) {
        System.out.println( "\nThank you for using our system." );
        System.out.printf( "%s, you are not logged out...\n", name );
    }

} // end class User