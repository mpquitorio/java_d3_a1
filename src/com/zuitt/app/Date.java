package com.zuitt.app;
// composition
public class Date {
    private final int month;
    private final int day;
    private final int year;

    // constructor
    public Date( int dMonth, int dDay, int dYear) {
        month = dMonth;
        day = dDay;
        year = dYear;
    }

    // return a String
    public String displayDate(){
        return String.format( "%d/%d/%d", month, day, year );
    }
}
